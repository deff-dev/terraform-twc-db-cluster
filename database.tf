locals {
  users = [
    for user in var.users : {
      username    = user.username
      password    = random_password.db_admin_user_password[user.username].result
      description = user.description
    }
  ]
  databases = flatten([
    for database in var.databases : [
      for name in database.name : {
        name        = name
        description = database.description
      }
    ]
  ])
}

data "twc_database_preset" "preset" {
  location = var.preset.location
  type     = var.type
  disk     = var.preset.disk_size

  dynamic "price_filter" {
    for_each = coalesce([var.preset.price], [])

    content {
      from = price_filter.value.min
      to   = price_filter.value.max
    }
  }
}

resource "twc_database_cluster" "db_cluster" {
  name       = var.name
  project_id = var.project_id
  preset_id  = data.twc_database_preset.preset.id
  type       = var.type
  hash_type  = var.hash_type

  config_parameters = var.parameters

  lifecycle {
    precondition {
      condition     = contains(["mysql5", "mysql", "postgres"], var.type) || var.hash_type == null
      error_message = "Error! Hash type is not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15", "mysql"], var.type) || var.parameters["max_connections"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql5", "mysql"], var.type) || var.parameters["auto_increment_increment"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql5", "mysql"], var.type) || var.parameters["auto_increment_offset"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql5", "mysql"], var.type) || var.parameters["innodb_io_capacity"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql5", "mysql"], var.type) || var.parameters["innodb_purge_threads"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql5", "mysql"], var.type) || var.parameters["innodb_read_io_threads"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql5", "mysql"], var.type) || var.parameters["innodb_thread_concurrency"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql5", "mysql"], var.type) || var.parameters["innodb_write_io_threads"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql5", "mysql"], var.type) || var.parameters["join_buffer_size"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql5", "mysql"], var.type) || var.parameters["max_allowed_packet"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql5", "mysql"], var.type) || var.parameters["max_heap_table_size"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["autovacuum_analyze_scale_factor"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["bgwriter_delay"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["bgwriter_lru_maxpages"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["deadlock_timeout"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["idle_in_transaction_session_timeout"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["idle_session_timeout"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["join_collapse_limit"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["lock_timeout"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["lock_timeout"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["max_prepared_transactions"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["shared_buffers"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["wal_buffers"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["temp_buffers"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["postgres", "postgres14", "postgres15"], var.type) || var.parameters["work_mem"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql"], var.type) || var.parameters["sql_mode"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql"], var.type) || var.parameters["query_cache_type"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
    precondition {
      condition     = contains(["mysql"], var.type) || var.parameters["query_cache_size"] == null
      error_message = "Error! Parameters not supported for the specified database type"
    }
  }
}

resource "twc_database_instance" "default" {
  for_each = { for database in local.databases : database.name => database }

  cluster_id = twc_database_cluster.db_cluster.id

  name        = each.value.name
  description = each.value.description
}

resource "random_password" "db_admin_user_password" {
  for_each = { for user in var.users : user.username => user }

  length  = each.value.password_length
  special = true
  # Other symbols are not allowed according to support
  override_special = "!()-+:?"

  lifecycle {
    precondition {
      condition     = each.value.password_length <= 30
      error_message = "Error! Password length cannot exceed 30 characters"
    }
  }
}

resource "twc_database_user" "db_admin" {
  for_each = { for user in local.users : user.username => user }

  cluster_id = twc_database_cluster.db_cluster.id

  login       = each.value.username
  password    = each.value.password
  description = each.value.description

  # Add when TW fix privillegies 
  privileges = []

  lifecycle {
    precondition {
      condition     = length(each.value.username) >= 3 && length(each.value.username) <= 30 && can(regex("^[a-z0-9]+$", each.value.username))
      error_message = "Error! Username length must be between 3 and 30 characters and contain only latin letters and numbers in lowercase"
    }
    precondition {
      condition     = length(each.value.password) >= 8 && length(each.value.password) <= 30
      error_message = "Error! Password length must be between 8 and 30 characters"
    }
  }
}